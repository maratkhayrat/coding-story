package com.epam.codings;
public class grouping {
        public static final String EXAMPLE_TEST = "Hello"
                + "this is made " + "with love.";

        public static void main(String[] args) {
            System.out.println(EXAMPLE_TEST.matches("\\w.*"));
            String[] splitString = (EXAMPLE_TEST.split("\\s+"));
            System.out.println(splitString.length);// should be 14
            for (String string : splitString) {
                System.out.println(string);
            }
            // replace all whitespace with tabs
            System.out.println(EXAMPLE_TEST.replaceAll("\\s+", "\t"));
        }
    }