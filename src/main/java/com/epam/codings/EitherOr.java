package com.epam.codings;
import java.util.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EitherOr {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s;
        Pattern pattern = Pattern.compile("(\\+359)(\\s+|-)(2)(\\2)([\\d]{3})(\\2)([\\d]{4}\\b)");

        while (!(s = scanner.nextLine()).equals("end")) {
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                System.out.println(s);
            }
        }
    }
}