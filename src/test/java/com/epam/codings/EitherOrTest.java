package com.epam.codings;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

       class EitherOrTest {
           private EitherOr s;
        @Test
        public void testSimpleTrue() {

            String s = "hakunamatata pumba timon";
            assertTrue(s.matches(".*(pumba|timon).*"));
            s = "hakunamatata pumba";
            assertFalse(s.matches(".*(pumba|timon).*"));
            s = "hakunamatata timon";
            assertTrue(s.matches(".*(pumba|timon).*"));
            s = "hakunamatata timon pumba";
            assertTrue(s.matches(".*(pumba|timon).*"));
        }
    }
